//
//  ViewController.m
//  dsf
//
//  Created by Wladyslaw Surala on 31.03.17.
//  Copyright © 2017 wladyslaws. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self printNumbers];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"\n");
        [self printSwitchedNumbers];
    });
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)printNumbers {
    NSLog(@"1");

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"2");
        });

    });
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
        NSLog(@"3");
    });
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
        NSLog(@"4");
    });

    NSLog(@"5") ;
}

- (void)printSwitchedNumbers{
    NSLog(@"1");

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"2");
        });

    });
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0), ^{
        NSLog(@"3");
    });
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^{
        NSLog(@"4");
    });

    NSLog(@"5") ;
}
@end
