//
//  main.m
//  dsf
//
//  Created by Wladyslaw Surala on 31.03.17.
//  Copyright © 2017 wladyslaws. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }


}
